export default{
    setCategories(state, payload){
        state.categories = payload
      },
      addCategory(state,payload){
        state.categories.push(payload)
      },
      editCategory(state, payload){
        const index = state.categories.findIndex(category => category.id == payload[0].id);
        state.categories[index].name = payload[0].name;
        state.categories[index].value = payload[0].value
      },
      removeCategory(state, id){
        const index = state.categories.findIndex(category => category.id == id);
        state.categories.splice(index, 1);
      },
}