const axios = require('axios').default;
const config = require('../config') 

export default{
    async getAllCategories(ctx){
        const response = await axios.get(`${config.default.baseURL}/cat`);
        ctx.commit('setCategories', response.data)
      },
}